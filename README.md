# sample-weather-project website
It get current weather data from OpenWeatherMap.org and user can get the weater data for the following cities: 
- London
- Hong Kong

# How to Run - using the Maven plugin
```
mvn spring-boot:run
```

# URL 
```
http://localhost:8080/web
```
